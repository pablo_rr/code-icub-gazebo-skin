import gym
import yarp
import time
import numpy as np
from gym import error, spaces, utils
from gym.utils import seeding

import icub

def dcm2axis(dcm):
  trace = np.trace(dcm)
  angle = np.arccos((trace - 1) / 2)
  if angle == 0:
    axis = np.array([1, 0, 0])
  elif angle == np.pi:
    # any axis can be used for pi rotation
    axis = np.array([0, 0, 1])
  else:
    skew = (dcm - dcm.T) / (2 * np.sin(angle))
    axis = np.array([skew[2, 1], skew[0, 2], skew[1, 0]])
  return np.concatenate([axis, [angle]])

def axis2dcm(axis, angle):
  c = np.cos(angle)
  s = np.sin(angle)
  t = 1 - c
  x, y, z = axis
  dcm = np.array([[t * x**2 + c, t * x*y - z*s, t * x*z + y*s],
                  [t * x*y + z*s, t * y**2 + c, t * y*z - x*s],
                  [t * x*z - y*s, t * y*z + x*s, t * z**2 + c]])
  return dcm

class Vector(yarp.Vector):
  def __init__(self, vec=None):
    if isinstance(vec, yarp.Vector):
      super().__init__(vec.length())
      for i in range(vec.length()):
        self[i] = vec[i]
    elif isinstance(vec, np.ndarray):
      super().__init__(vec.shape[0])
      for i in range(vec.shape[0]):
        self[i] = vec[i]
    elif isinstance(vec, (list, int)):
      super().__init__(vec)
    else:
      super().__init__()
            
  def __add__(self, const):
    return Vector([self[i] + const for i in range(self.length())])
  
  def __mul__(self, const):
    return Vector([self[i] * const for i in range(self.length())])
  
  def __repr__(self):
    arr = [str(self[i]) for i in range(self.length())]
    string = ", ".join(arr)
    return "[" + string + "]"

  def to_numpy(self):
    vec = np.zeros(self.length())
    for i in range(self.length()):
      vec[i] = self[i]
    return vec

class Matrix(yarp.Matrix):
  def __init__(self, mat):
    if isinstance(mat, yarp.Matrix):
      super().__init__(mat.rows(), mat.cols())
      for i in range(mat.rows()):
        self.setRow(i, mat.getRow(i))
    elif isinstance(mat, np.ndarray):
      super().__init__(mat.shape[0], mat.shape[1])
      for (i, row) in enumerate(mat):
        self.setRow(i, Vector(row.tolist()))
      
  def __repr__(self):
    output = ""
    mat = self.to_numpy()
    for i in range(mat.shape[0]):
      for j in range(mat.shape[1]):
        output += f"{mat[i][j]:10.4f} "
      output += "\n"
    return output
  
  def from_numpy(self, mat):
    ymat = Matrix(mat.shape[0], mat.shape[1])
    for (i, row) in enumerate(mat):
      ymat.setRow(i, Vector(row.tolist()))
    return ymat
  
  def to_numpy(self):
    mat = np.zeros([self.rows(), self.cols()])
    for i in range(self.rows()):
      row = self.getRow(i)
      mat[i, :] = [row[j] for j in range(row.length())]
    return mat

class IcubSkinEnv(gym.Env):
  """
  Description:
    This environment connects to Gazebo simulation of iCub robot with
    artificial skin over YARP ports. The goal is to explore as much
    skin surface as possible and build an efficient model of robot's
    body using the sensation of touch.

  Observation:
    Dict({
      # Joint states
      "joints": Dict({
        "head": Box(6),
        "left_arm": Box(16),
        "right_arm": Box(16),
        "torso": Box(3)
      }),
      
      # Taxel activations
      "skin": Dict({
        "left_arm": Box(768),
        "left_forearm": Box(384),
        "left_hand": Box(192),
        "right_arm": Box(768),
        "right_forearm": Box(384),
        "right_hand": Box(192),
        "torso": Box(768)
      }),
      
      # Touch coordinates on skin patches
      # (0, 0) means "no touch"
      "touch": Dict({
        "left_arm": Box(2),
        "left_forearm": Box(2),
        "left_hand": Box(2),
        "right_arm": Box(2),
        "right_forearm": Box(2),
        "right_hand": Box(2),
        "torso": Box(2)
      })
    })

  Actions:
    Dict({
      "head": Box(6),
      "left_arm": Box(16),
      "right_arm": Box(16),
      "torso": Box(3)
    })

  Reward:
    Reward is 1 for every newly discovered taxel

  Starting State:
    Starting state is dictated by Gazebo simulation
	
  Episode Termination:
    Episode length is greater than 1000

  Solved Requirements:
    Considered solved when more than 80% of skin is explored
    and an efficient body model is learned that is able to
    reproduce a set of test touches
  """

  metadata = {
    "activeBodyParts": ["head", "left_arm", "right_arm", "torso"],
    "jointSpeed": 40,
    "maxEpisodeSteps": 1000,
    "maxMoveDuration": 15.0,
    "moveTerminationEpsilon": 0.2,
    "moveTimeStep": 0.25,
    "render.modes": ['terminal']
  }

  def __init__(self):
    # Number of taxels per body part
    self.N_TAXELS_TORSO = 768
    self.N_TAXELS_ARM = 768
    self.N_TAXELS_FOREARM = 384
    self.N_TAXELS_HAND = 192

    # Number of motors per body part
    self.N_MOTORS_ARM = 16
    self.N_MOTORS_HEAD = 6
    self.N_MOTORS_TORSO = 3

    self.action_cmd = 'motors'

    """
    Body parts config, format: [
      Body part name,
      Number of taxels per body part,
      Input port name,
      Output port name
    ]
    """
    self.BODY_PARTS = [
      ["left_arm",      self.N_TAXELS_ARM,     "/gym_icub_skin/left_arm:i",      "/icubSim/skin/left_arm_comp"     ],
      ["left_forearm",  self.N_TAXELS_FOREARM, "/gym_icub_skin/left_forearm:i",  "/icubSim/skin/left_forearm_comp" ],
      ["left_hand",     self.N_TAXELS_HAND,    "/gym_icub_skin/left_hand:i",     "/icubSim/skin/left_hand_comp"    ],
      ["right_arm",     self.N_TAXELS_ARM,     "/gym_icub_skin/right_arm:i",     "/icubSim/skin/right_arm_comp"    ],
      ["right_forearm", self.N_TAXELS_FOREARM, "/gym_icub_skin/right_forearm:i", "/icubSim/skin/right_forearm_comp"],
      ["right_hand",    self.N_TAXELS_HAND,    "/gym_icub_skin/right_hand:i",    "/icubSim/skin/right_hand_comp"   ],
      ["torso",         self.N_TAXELS_TORSO,   "/gym_icub_skin/torso:i",         "/icubSim/skin/torso_comp"        ]
    ]

    """
    Motors config, format: [
      Body part name,
      Number of joint,
      Remote name,
      Local port name
    ]
    """
    self.MOTORS = [
      ["head",      self.N_MOTORS_HEAD,  "/icubSim/head",      "/gym_icub_skin/client/head"     ],
      ["left_arm",  self.N_MOTORS_ARM,   "/icubSim/left_arm",  "/gym_icub_skin/client/left_arm" ],
      ["right_arm", self.N_MOTORS_ARM,   "/icubSim/right_arm", "/gym_icub_skin/client/right_arm"],
      ["torso",     self.N_MOTORS_TORSO, "/icubSim/torso",     "/gym_icub_skin/client/torso"    ]
    ]

    # TODO: make sure if adding pose registers is suitable or not for the application. Not a priority

    # Home pose
    home_arm   = [-30, 30, 0, 45, 0, 0, 0, 60, 0, 0, 0, 0, 0, 0, 0, 0]
    home_head  = [0, 0, 0, 0, 0, 0]
    home_torso = [0, 0, 0]
    home_effector = [-0.08578, -0.00658, 0.11984, -0.94642, -0.11927, 0.3, 1.80405]
    self.HOME_POSE = {
      "head":      np.double(np.array(home_head)),
      "left_arm":  np.double(np.array(home_arm)),
      "right_arm": np.double(np.array(home_arm)),
      "torso":     np.double(np.array(home_torso)),
      "effector":  np.double(np.array(home_effector))
    }

    # TODO: check if a HOME POSE variable for end effector is suitable. Not a priority
    
    # Read files with taxel coordinates on skin patches
    self.SKIN_COORDINATES = [
      ["left_arm",      [0] * self.N_TAXELS_ARM,     [0] * self.N_TAXELS_ARM     ],
      ["left_forearm",  [0] * self.N_TAXELS_FOREARM, [0] * self.N_TAXELS_FOREARM ],
      ["left_hand",     [0] * self.N_TAXELS_HAND,    [0] * self.N_TAXELS_HAND    ],
      ["right_arm",     [0] * self.N_TAXELS_ARM,     [0] * self.N_TAXELS_ARM     ],
      ["right_forearm", [0] * self.N_TAXELS_FOREARM, [0] * self.N_TAXELS_FOREARM ],
      ["right_hand",    [0] * self.N_TAXELS_HAND,    [0] * self.N_TAXELS_HAND    ],
      ["torso",         [0] * self.N_TAXELS_TORSO,   [0] * self.N_TAXELS_TORSO   ]
    ]
    for sc in self.SKIN_COORDINATES:
      filename = './coordinates/' + sc[0] + '.txt'
      f = open(filename)
      for line in f:
        i, x, y = [int(x) for x in line.split()];
        sc[1][i] = x
        sc[2][i] = y
      f.close()

    # Initialize YARP network
    yarp.Network.init()

    # Open and connect ports for each body part
    self.input_ports = []
    for bp in self.BODY_PARTS:
      port = yarp.BufferedPortBottle()
      port.open(bp[2])
      yarp.Network.connect(bp[3], bp[2])
      self.input_ports.append(port);

    # Observation space
    # Joint limits from /usr/share/iCub/contexts/simConfig/*.ini
    arm_low    = [-95,     0, -37, 15.5, -90, -90, -20,  0, 10,  0,   0,  0,   0,  0,   0,   0]
    arm_high   = [ 10, 160.8,  80,  106,  90,   0,  40, 60, 90, 90, 180, 90, 180, 90, 180, 270]
    head_low   = [-40, -70, -55, -35, -50, -0]
    head_high  = [ 30,  60,  55,  15,  52, 90]
    torso_low  = [-50, -30, -10]
    torso_high = [ 50,  30,  70]
    self.observation_space = spaces.Dict({
      "joints": spaces.Dict({
        "head": spaces.Box(
          low= np.double(np.array(head_low)),
          high=np.double(np.array(head_high)),
          dtype=np.double
        ),
        "left_arm": spaces.Box(
          low= np.double(np.array(arm_low)),
          high=np.double(np.array(arm_high)),
          dtype=np.double
        ),
        "right_arm": spaces.Box(
          low= np.double(np.array(arm_low)),
          high=np.double(np.array(arm_high)),
          dtype=np.double
        ),
        "torso": spaces.Box(
          low= np.double(np.array(torso_low)),
          high=np.double(np.array(torso_high)),
          dtype=np.double
        )
      }),
      "skin": spaces.Dict({
        "left_arm": spaces.Box(
          low= np.double(np.array([0] * self.N_TAXELS_ARM)),
          high=np.double(np.array([256] * self.N_TAXELS_ARM)),
          dtype=np.double
        ),
        "left_forearm": spaces.Box(
          low= np.double(np.array([0] * self.N_TAXELS_FOREARM)),
          high=np.double(np.array([256] * self.N_TAXELS_FOREARM)),
          dtype=np.double
        ),
        "left_hand": spaces.Box(
          low= np.double(np.array([0] * self.N_TAXELS_HAND)),
          high=np.double(np.array([256] * self.N_TAXELS_HAND)),
          dtype=np.double
        ),
        "right_arm": spaces.Box(
          low= np.double(np.array([0] * self.N_TAXELS_ARM)),
          high=np.double(np.array([256] * self.N_TAXELS_ARM)),
          dtype=np.double
        ),
        "right_forearm": spaces.Box(
          low= np.double(np.array([0] * self.N_TAXELS_FOREARM)),
          high=np.double(np.array([256] * self.N_TAXELS_FOREARM)),
          dtype=np.double
        ),
        "right_hand": spaces.Box(
          low= np.double(np.array([0] * self.N_TAXELS_HAND)),
          high=np.double(np.array([256] * self.N_TAXELS_HAND)),
          dtype=np.double
        ),
        "torso": spaces.Box(
          low= np.double(np.array([0] * self.N_TAXELS_TORSO)),
          high=np.double(np.array([256] * self.N_TAXELS_TORSO)),
          dtype=np.double
        )
      }),
      "touch": spaces.Dict({
        # left_arm unused
        "left_arm": spaces.Box(
          low= np.double(np.array([0, 0])),
          high=np.double(np.array([0, 0])),
          dtype=np.double
        ),
        "left_forearm": spaces.Box(
          low= np.double(np.array([450, 600])),
          high=np.double(np.array([0, 0])),
          dtype=np.double
        ),
        "left_hand": spaces.Box(
          low= np.double(np.array([0, 0])),
          high=np.double(np.array([640, 640])),
          dtype=np.double
        ),
        # right_arm unused
        "right_arm": spaces.Box(
          low= np.double(np.array([0, 0])),
          high=np.double(np.array([0, 0])),
          dtype=np.double
        ),
        "right_forearm": spaces.Box(
          low= np.double(np.array([0, 0])),
          high=np.double(np.array([450, 600])),
          dtype=np.double
        ),
        "right_hand": spaces.Box(
          low= np.double(np.array([0, 0])),
          high=np.double(np.array([640, 640])),
          dtype=np.double
        ),
        "torso": spaces.Box(
          low= np.double(np.array([0, 0])),
          high=np.double(np.array([500, 500])),
          dtype=np.double
        )
      }),
      "effector_pose": spaces.Box(
          low= np.double(np.array([-50] * 7)),
          high=np.double(np.array([50] * 7)),
          dtype=np.double
        )
    })

    # Action space
    # TODO: add end effector action bounds
    self.action_space = spaces.Dict({
      "head": spaces.Box(
        low= np.double(np.array(head_low)),
        high=np.double(np.array(head_high)),
        dtype=np.double
      ),
      "left_arm": spaces.Box(
        low= np.double(np.array(arm_low)),
        high=np.double(np.array(arm_high)),
        dtype=np.double
      ),
      "right_arm": spaces.Box(
        low= np.double(np.array(arm_low)),
        high=np.double(np.array(arm_high)),
        dtype=np.double
      ),
      "torso": spaces.Box(
        low= np.double(np.array(torso_low)),
        high=np.double(np.array(torso_high)),
        dtype=np.double
      ),
      "end_effector": spaces.Box(
        low = np.double(np.array([-0.2] * 7)),
        high = np.double(np.array([0.2] * 7)),
        dtype = np.double
      )
    })

    # Initialize motors
    self.motor_drivers = []
    for m in self.MOTORS:
      props = yarp.Property()
      props.put("device", "remote_controlboard")
      props.put("local", m[3])
      props.put("remote", m[2])

      driver = yarp.PolyDriver(props)
      iPos = driver.viewIPositionControl()
      iVel = driver.viewIVelocityControl()
      iEnc = driver.viewIEncoders()

      self.motor_drivers.append({
        "driver": driver,
        "iPos": iPos,
        "iVel": iVel,
        "iEnc": iEnc
      })

    # TODO: add cartesian solver driver and end effector initialization
    props = yarp.Property()
    props.put("device", "cartesiancontrollerclient")
    props.put("remote", "/icubSim/cartesianController/left_arm")
    props.put("local", "/gym_icub_skin/cartesian_client/left_arm")

    self.cartesian_driver = yarp.PolyDriver(props)
    self.iarm = self.cartesian_driver.viewICartesianControl()
    self.iarm.setInTargetTol(float(0.001))

    nencs = self.motor_drivers[1]['iEnc'].getAxes()
    encs = Vector(nencs)
    self.motor_drivers[1]['iEnc'].getEncoders(encs.data())

    joints = Vector()
    thumb = icub.iCubFinger("left_thumb")
    thumb.getChainJoints(encs, joints)
    tipframe = thumb.getH(joints * (np.pi / 180.0))

    self.tip_x = tipframe.getCol(3)
    self.tip_o = Vector(dcm2axis(Matrix(tipframe).to_numpy()))

    self.gazebo_control_port = yarp.RpcClient()
    self.gazebo_control_port.open("/reset_ctrl")
    yarp.Network.connect("/reset_ctrl", "/clock/rpc")

    self.icontrol = [drv['driver'].viewIControlMode() for drv in self.motor_drivers]

    # Track episode termination
    self.step_counter = 0

    # Reset reward lookup table
    self.reset_reward_table()

    return

  # Reset reward lookup table - set false for each taxel
  def reset_reward_table(self):
    self.reward_lookup_table = {}
    for bp in self.BODY_PARTS:
      name = bp[0]
      n_taxels = bp[1]
      self.reward_lookup_table[name] = [False for i in range(n_taxels)]
  
  # Get Center-of-Mass of activated taxels for a body part
  # Here "mass" is taxel activation strength (0..255)
  def get_com(self, taxels, skin_coordinates):
    total_mass = 0
    total_x = 0
    total_y = 0
    for i in range(len(taxels)):
      mass = taxels[i]
      total_mass += mass
      total_x += mass * skin_coordinates[1][i]
      total_y += mass * skin_coordinates[2][i]
    if total_mass == 0:
      return [0, 0]
    return [total_x / total_mass, total_y / total_mass]

  def step(self, action):
    # TODO: check if the environment is setted to work over the motor angles or by end effector pose
    # TODO: if action is commanded by end effector pose, modify current pose by the position commanded
    # TODO: one desirable aspect is to maintain the end effector angle pointing to the back of the robot

    # Apply action
    if self.action_cmd == 'motors':
      self.setMotorPose(action)
    else: #'effector'
      x0, o0 = Vector(3), Vector(4)
      self.iarm.getPose(x0, o0)
      x1, o1 = Vector(x0.to_numpy() + action[:3]), Vector(o0.to_numpy() + action[3:]) # don't modify orientation for now
      self.iarm.goToPose(x1, o1)
      timer = 0
      dt = 0.5
      done = False
      while done is not True:
        done = self.iarm.checkMotionDone()
        if timer > 5:
          break
        time.sleep(dt)
        timer += dt
      self.iarm.stopControl()

    # Prepare observation
    observation = {
      "joints": {
        "head": [],
        "left_arm": [],
        "right_arm": [],
        "torso": []
      },
      "skin": {
        "left_arm": [],
        "left_forearm": [],
        "left_hand": [],
        "right_arm": [],
        "right_forearm": [],
        "right_hand": [],
        "torso": []
      },
      "touch": {
        "left_arm": [0, 0],
        "left_forearm": [0, 0],
        "left_hand": [0, 0],
        "right_arm": [0, 0],
        "right_forearm": [0, 0],
        "right_hand": [0, 0],
        "torso": [0, 0]
      },
      "effector_pose": np.zeros(7)
    }
    
    for i in range(len(self.MOTORS)):
      m = self.MOTORS[i]
      motorName = m[0]
      numJoints = m[1]
      encs = yarp.Vector(numJoints)
      self.motor_drivers[i]['iEnc'].getEncoders(encs.data())
      observation['joints'][motorName] = np.array([encs[j] for j in range(numJoints)])

    for i in range(len(self.BODY_PARTS)):
      bp = self.BODY_PARTS[i]
      btl = self.input_ports[i].read(True)
      observation['skin'][bp[0]] = [btl.get(j).asFloat64() for j in range(btl.size())]
      observation['touch'][bp[0]] = self.get_com(observation['skin'][bp[0]], self.SKIN_COORDINATES[i])

    # TODO: add end effector pose as part of the observation set
    if self.action_cmd == 'effector':
      x0, o0 = Vector(3), Vector(4)
      self.iarm.getPose(x0, o0)
      observation['effector_pose'] = np.concatenate([x0.to_numpy(), o0.to_numpy()])

    # Calculate solved condition
    self.step_counter += 1
    done = self.step_counter >= self.metadata['maxEpisodeSteps']

    # Calculate reward
    reward = 0
    for bp in self.BODY_PARTS:
      name = bp[0]
      n_taxels = bp[1]
      for i in range(n_taxels):
        if (not self.reward_lookup_table[name][i]) and observation['skin'][name][i]:
          self.reward_lookup_table[name][i] = True
          reward += 1

    return observation, reward, done, {}


  def reset(self, seed = None, options = None):
    # Set movement speeds for all joints
    for i in range(len(self.MOTORS)):
      m = self.MOTORS[i]
      numJoints = m[1]
      speeds = yarp.Vector(numJoints)
      for j in range(numJoints):
        speeds[j] = self.metadata['jointSpeed'];
      self.motor_drivers[i]['iPos'].setRefSpeeds(speeds.data())

    # Move robot to home pose
    # Temporary solution
    action = self.HOME_POSE
    def home_pose():
      return [-60, 40, 80, 105, -60, 24, 0, 60, 90, 0, 0, 0, 0, 0, 0, 0]
    action['left_arm'] = np.double(np.array(home_pose()))
    self.setMotorPose(self.HOME_POSE)

    if self.action_cmd == 'effector':
      self.iarm.goToPose(Vector(self.HOME_POSE['effector'][:3]), Vector(self.HOME_POSE['effector'][3:]))
      timer = 0
      dt = 0.5
      done = False
      while done is not True:
        done = self.iarm.checkMotionDone()
        if timer > 5:
          break
        time.sleep(dt)
        timer += dt
      self.iarm.stopControl()

    # Reset step counter
    self.step_counter = 0

    # Reset reward lookup table
    self.reset_reward_table()
    #return {}, {}


  # Dummy method
  # Rendering is actually done in Gazebo simulator
  def render(self, mode='terminal'):
    # TODO: maybe print joint values and some skin information to terminal here?
    pass

  def close(self):
    # Disconnect ports
    for bp in self.BODY_PARTS:
      yarp.Network.disconnect(bp[3], bp[2])
    for m in self.MOTORS:
      yarp.Network.disconnect(m[3], m[2])
    for m in self.motor_drivers:
      m['driver'].close()
    for ip in self.input_ports:
      ip.close()

    yarp.Network.disconnect("/icubSim/cartesianController/left_arm", "/gym_icub_skin/cartesian_client/left_arm")
    self.cartesian_driver.close()

    reset_cmd = yarp.Bottle()
    reset_cmd.addString("pauseSimulation")
    self.gazebo_control_port.write(reset_cmd, reset_cmd)
    time.sleep(1.0)
    reset_cmd.clear()
    reset_cmd.addString("resetSimulation")
    self.gazebo_control_port.write(reset_cmd, reset_cmd)
    time.sleep(1.0)
    reset_cmd.clear()
    reset_cmd.addString("continueSimulation")
    self.gazebo_control_port.write(reset_cmd, reset_cmd)
    yarp.Network.disconnect("/reset_ctrl", "/clock/rpc")
    self.gazebo_control_port.close()

    yarp.Network.fini()
    return

  def reset_simulation(self):
    self.set_action_cmd('motors')

    modes = yarp.VectorInt(16)
    for ic in self.icontrol:
      ic.getControlModes(modes.data())
      for i in range(modes.length()):
        modes[i] = yarp.VOCAB_CM_POSITION
      ic.setControlModes(modes.data())

    reset_cmd = yarp.Bottle()
    reset_cmd.addString("pauseSimulation")
    self.gazebo_control_port.write(reset_cmd, reset_cmd)
    time.sleep(1.0)
    reset_cmd.clear()
    reset_cmd.addString("resetSimulation")
    self.gazebo_control_port.write(reset_cmd, reset_cmd)
    time.sleep(1.0)
    reset_cmd.clear()
    reset_cmd.addString("continueSimulation")
    self.gazebo_control_port.write(reset_cmd, reset_cmd)
    time.sleep(1.0)

  def setMotorPose(self, action):
    # Apply action - move motors
    targets = []
    for i in range(len(self.MOTORS)):
      m = self.MOTORS[i]
      motorName = m[0]
      numJoints = m[1]
      if motorName in self.metadata['activeBodyParts']:
        angles = yarp.Vector(numJoints)
        for j in range(numJoints):
          angles[j] = action[motorName][j]
        targets.append(np.array(action[motorName]))
        self.motor_drivers[i]['iPos'].positionMove(angles.data())

    # Wait until movement is stopped or maxMoveDuration seconds expire
    timer = 0
    dt = self.metadata['moveTimeStep']
    
    # Store previous motor state
    v_prev = []
    for i in range(len(self.MOTORS)):
      m = self.MOTORS[i]
      motorName = m[0]
      numJoints = m[1]
      if motorName in self.metadata['activeBodyParts']:
        encs = yarp.Vector(numJoints)
        self.motor_drivers[i]['iEnc'].getEncoders(encs.data())
        v_prev.append(np.array([encs[j] for j in range(numJoints)]))
      else:
        v_prev.append(None)

    while timer < self.metadata['maxMoveDuration']:
      time.sleep(dt)
      timer += dt
      all_good = True
      for i in range(len(self.MOTORS)):
        m = self.MOTORS[i]
        motorName = m[0]
        numJoints = m[1]
        if motorName in self.metadata['activeBodyParts']:
          encs = yarp.Vector(numJoints)
          self.motor_drivers[i]['iEnc'].getEncoders(encs.data())
          v = np.array([encs[j] for j in range(numJoints)])
          v_norm = np.linalg.norm(v - v_prev[i]) # (v - targets[i])
          if (v_norm > self.metadata['moveTerminationEpsilon']):
            all_good = False
          v_prev[i] = v
      if (all_good):
        break

  def get_obs(self):
    # Prepare observation
    observation = {
      "joints": {
        "head": [],
        "left_arm": [],
        "right_arm": [],
        "torso": []
      },
      "skin": {
        "left_arm": [],
        "left_forearm": [],
        "left_hand": [],
        "right_arm": [],
        "right_forearm": [],
        "right_hand": [],
        "torso": []
      },
      "touch": {
        "left_arm": [0, 0],
        "left_forearm": [0, 0],
        "left_hand": [0, 0],
        "right_arm": [0, 0],
        "right_forearm": [0, 0],
        "right_hand": [0, 0],
        "torso": [0, 0]
      },
      "effector_pose": np.zeros(7)
    }

    for i in range(len(self.MOTORS)):
      m = self.MOTORS[i]
      motorName = m[0]
      numJoints = m[1]
      encs = yarp.Vector(numJoints)
      self.motor_drivers[i]['iEnc'].getEncoders(encs.data())
      observation['joints'][motorName] = np.array([encs[j] for j in range(numJoints)])

    for i in range(len(self.BODY_PARTS)):
      bp = self.BODY_PARTS[i]
      btl = self.input_ports[i].read(True)
      observation['skin'][bp[0]] = [btl.get(j).asFloat64() for j in range(btl.size())]
      observation['touch'][bp[0]] = self.get_com(observation['skin'][bp[0]], self.SKIN_COORDINATES[i])

    if self.action_cmd == 'effector':
      x0, o0 = Vector(3), Vector(4)
      self.iarm.getPose(x0, o0)
      observation['effector_pose'] = np.concatenate([x0.to_numpy(), o0.to_numpy()])

    return observation

  def set_action_cmd(self, action_cmd):
    self.action_cmd = action_cmd

# Test
if __name__ == "__main__":
  env = IcubSkinEnv()
  env.reset()
  observation, reward, done, info = env.step({
    "head":      np.double(np.array([0, 0, 0, 0, 0, 0])),
    "left_arm":  np.double(np.array([-30, 30, 0, 60, 0, 0, 0, 60, 0, 0, 0, 0, 0, 0, 0, 0])),
    "right_arm": np.double(np.array([-30, 30, 0, 45, 0, 0, 0, 60, 0, 0, 0, 0, 0, 0, 0, 0])),
    "torso":     np.double(np.array([0, 0, 0]))
  })
  print(observation)

