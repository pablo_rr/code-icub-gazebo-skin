import gym
import yarp
import time
import numpy as np
from gym import error, spaces, utils
from gym.utils import seeding

class IcubSkinEnv(gym.Env):
  """
  Description:
    This environment connects to Gazebo simulation of iCub robot with
    artificial skin over YARP ports. The goal is to explore as much
    skin surface as possible and build an efficient model of robot's
    body using the sensation of touch.

  Observation:
    Dict({
      # Joint states
      "joints": Dict({
        "head": Box(6),
        "left_arm": Box(16),
        "right_arm": Box(16),
        "torso": Box(3)
      }),
      
      # Taxel activations
      "skin": Dict({
        "left_arm": Box(768),
        "left_forearm": Box(384),
        "left_hand": Box(192),
        "right_arm": Box(768),
        "right_forearm": Box(384),
        "right_hand": Box(192),
        "torso": Box(768)
      }),
      
      # Touch coordinates on skin patches
      # (0, 0) means "no touch"
      "touch": Dict({
        "left_arm": Box(2),
        "left_forearm": Box(2),
        "left_hand": Box(2),
        "right_arm": Box(2),
        "right_forearm": Box(2),
        "right_hand": Box(2),
        "torso": Box(2)
      })
    })

  Actions:
    Dict({
      "head": Box(6),
      "left_arm": Box(16),
      "right_arm": Box(16),
      "torso": Box(3)
    })

  Reward:
    Reward is 1 for every newly discovered taxel

  Starting State:
    Starting state is dictated by Gazebo simulation
	
  Episode Termination:
    Episode length is greater than 1000

  Solved Requirements:
    Considered solved when more than 80% of skin is explored
    and an efficient body model is learned that is able to
    reproduce a set of test touches
  """

  metadata = {
    "activeBodyParts": ["head", "left_arm", "right_arm", "torso"],
    "jointSpeed": 50,
    "maxEpisodeSteps": 1000,
    "maxMoveDuration": 15.0,
    "moveTerminationEpsilon": 0.2,
    "moveTimeStep": 0.25,
    "render.modes": ['terminal']
  }

  def __init__(self):
    # Number of taxels per body part
    self.N_TAXELS_TORSO = 768
    self.N_TAXELS_ARM = 768
    self.N_TAXELS_FOREARM = 384
    self.N_TAXELS_HAND = 192

    # Number of motors per body part
    self.N_MOTORS_ARM = 16
    self.N_MOTORS_HEAD = 6
    self.N_MOTORS_TORSO = 3

    """
    Body parts config, format: [
      Body part name,
      Number of taxels per body part,
      Input port name,
      Output port name
    ]
    """
    self.BODY_PARTS = [
      ["left_arm",      self.N_TAXELS_ARM,     "/gym_icub_skin/left_arm:i",      "/icubSim/skin/left_arm_comp"     ],
      ["left_forearm",  self.N_TAXELS_FOREARM, "/gym_icub_skin/left_forearm:i",  "/icubSim/skin/left_forearm_comp" ],
      ["left_hand",     self.N_TAXELS_HAND,    "/gym_icub_skin/left_hand:i",     "/icubSim/skin/left_hand_comp"    ],
      ["right_arm",     self.N_TAXELS_ARM,     "/gym_icub_skin/right_arm:i",     "/icubSim/skin/right_arm_comp"    ],
      ["right_forearm", self.N_TAXELS_FOREARM, "/gym_icub_skin/right_forearm:i", "/icubSim/skin/right_forearm_comp"],
      ["right_hand",    self.N_TAXELS_HAND,    "/gym_icub_skin/right_hand:i",    "/icubSim/skin/right_hand_comp"   ],
      ["torso",         self.N_TAXELS_TORSO,   "/gym_icub_skin/torso:i",         "/icubSim/skin/torso_comp"        ]
    ]

    """
    Motors config, format: [
      Body part name,
      Number of joint,
      Remote name,
      Local port name
    ]
    """
    self.MOTORS = [
      ["head",      self.N_MOTORS_HEAD,  "/icubSim/head",      "/gym_icub_skin/client/head"     ],
      ["left_arm",  self.N_MOTORS_ARM,   "/icubSim/left_arm",  "/gym_icub_skin/client/left_arm" ],
      ["right_arm", self.N_MOTORS_ARM,   "/icubSim/right_arm", "/gym_icub_skin/client/right_arm"],
      ["torso",     self.N_MOTORS_TORSO, "/icubSim/torso",     "/gym_icub_skin/client/torso"    ]
    ]

    # Home pose
    home_arm   = [-30, 30, 0, 45, 0, 0, 0, 60, 0, 0, 0, 0, 0, 0, 0, 0]
    home_head  = [0, 0, 0, 0, 0, 0]
    home_torso = [0, 0, 0]
    self.HOME_POSE = {
      "head":      np.double(np.array(home_head)),
      "left_arm":  np.double(np.array(home_arm)),
      "right_arm": np.double(np.array(home_arm)),
      "torso":     np.double(np.array(home_torso))
    }
    
    # Read files with taxel coordinates on skin patches
    self.SKIN_COORDINATES = [
      ["left_arm",      [0] * self.N_TAXELS_ARM,     [0] * self.N_TAXELS_ARM     ],
      ["left_forearm",  [0] * self.N_TAXELS_FOREARM, [0] * self.N_TAXELS_FOREARM ],
      ["left_hand",     [0] * self.N_TAXELS_HAND,    [0] * self.N_TAXELS_HAND    ],
      ["right_arm",     [0] * self.N_TAXELS_ARM,     [0] * self.N_TAXELS_ARM     ],
      ["right_forearm", [0] * self.N_TAXELS_FOREARM, [0] * self.N_TAXELS_FOREARM ],
      ["right_hand",    [0] * self.N_TAXELS_HAND,    [0] * self.N_TAXELS_HAND    ],
      ["torso",         [0] * self.N_TAXELS_TORSO,   [0] * self.N_TAXELS_TORSO   ]
    ]
    for sc in self.SKIN_COORDINATES:
      filename = './coordinates/' + sc[0] + '.txt'
      f = open(filename)
      for line in f:
        i, x, y = [int(x) for x in line.split()];
        sc[1][i] = x
        sc[2][i] = y
      f.close()

    # Initialize YARP network
    yarp.Network.init()

    # Open and connect ports for each body part
    self.input_ports = []
    for bp in self.BODY_PARTS:
      port = yarp.BufferedPortBottle()
      port.open(bp[2])
      yarp.Network.connect(bp[3], bp[2])
      self.input_ports.append(port);

    # Observation space
    # Joint limits from /usr/share/iCub/contexts/simConfig/*.ini
    arm_low    = [-95,     0, -37, 15.5, -90, -90, -20,  0, 10,  0,   0,  0,   0,  0,   0,   0]
    arm_high   = [ 10, 160.8,  80,  106,  90,   0,  40, 60, 90, 90, 180, 90, 180, 90, 180, 270]
    head_low   = [-40, -70, -55, -35, -50, -0]
    head_high  = [ 30,  60,  55,  15,  52, 90]
    torso_low  = [-50, -30, -10]
    torso_high = [ 50,  30,  70]
    self.observation_space = spaces.Dict({
      "joints": spaces.Dict({
        "head": spaces.Box(
          low= np.double(np.array(head_low)),
          high=np.double(np.array(head_high)),
          dtype=np.double
        ),
        "left_arm": spaces.Box(
          low= np.double(np.array(arm_low)),
          high=np.double(np.array(arm_high)),
          dtype=np.double
        ),
        "right_arm": spaces.Box(
          low= np.double(np.array(arm_low)),
          high=np.double(np.array(arm_high)),
          dtype=np.double
        ),
        "torso": spaces.Box(
          low= np.double(np.array(torso_low)),
          high=np.double(np.array(torso_high)),
          dtype=np.double
        )
      }),
      "skin": spaces.Dict({
        "left_arm": spaces.Box(
          low= np.double(np.array([0] * self.N_TAXELS_ARM)),
          high=np.double(np.array([256] * self.N_TAXELS_ARM)),
          dtype=np.double
        ),
        "left_forearm": spaces.Box(
          low= np.double(np.array([0] * self.N_TAXELS_FOREARM)),
          high=np.double(np.array([256] * self.N_TAXELS_FOREARM)),
          dtype=np.double
        ),
        "left_hand": spaces.Box(
          low= np.double(np.array([0] * self.N_TAXELS_HAND)),
          high=np.double(np.array([256] * self.N_TAXELS_HAND)),
          dtype=np.double
        ),
        "right_arm": spaces.Box(
          low= np.double(np.array([0] * self.N_TAXELS_ARM)),
          high=np.double(np.array([256] * self.N_TAXELS_ARM)),
          dtype=np.double
        ),
        "right_forearm": spaces.Box(
          low= np.double(np.array([0] * self.N_TAXELS_FOREARM)),
          high=np.double(np.array([256] * self.N_TAXELS_FOREARM)),
          dtype=np.double
        ),
        "right_hand": spaces.Box(
          low= np.double(np.array([0] * self.N_TAXELS_HAND)),
          high=np.double(np.array([256] * self.N_TAXELS_HAND)),
          dtype=np.double
        ),
        "torso": spaces.Box(
          low= np.double(np.array([0] * self.N_TAXELS_TORSO)),
          high=np.double(np.array([256] * self.N_TAXELS_TORSO)),
          dtype=np.double
        )
      }),
      "touch": spaces.Dict({
        # left_arm unused
        "left_arm": spaces.Box(
          low= np.double(np.array([0, 0])),
          high=np.double(np.array([0, 0])),
          dtype=np.double
        ),
        "left_forearm": spaces.Box(
          low= np.double(np.array([450, 600])),
          high=np.double(np.array([0, 0])),
          dtype=np.double
        ),
        "left_hand": spaces.Box(
          low= np.double(np.array([0, 0])),
          high=np.double(np.array([640, 640])),
          dtype=np.double
        ),
        # right_arm unused
        "right_arm": spaces.Box(
          low= np.double(np.array([0, 0])),
          high=np.double(np.array([0, 0])),
          dtype=np.double
        ),
        "right_forearm": spaces.Box(
          low= np.double(np.array([0, 0])),
          high=np.double(np.array([450, 600])),
          dtype=np.double
        ),
        "right_hand": spaces.Box(
          low= np.double(np.array([0, 0])),
          high=np.double(np.array([640, 640])),
          dtype=np.double
        ),
        "torso": spaces.Box(
          low= np.double(np.array([0, 0])),
          high=np.double(np.array([500, 500])),
          dtype=np.double
        )
      })
    })

    # Action space
    self.action_space = spaces.Dict({
      "head": spaces.Box(
        low= np.double(np.array(head_low)),
        high=np.double(np.array(head_high)),
        dtype=np.double
      ),
      "left_arm": spaces.Box(
        low= np.double(np.array(arm_low)),
        high=np.double(np.array(arm_high)),
        dtype=np.double
      ),
      "right_arm": spaces.Box(
        low= np.double(np.array(arm_low)),
        high=np.double(np.array(arm_high)),
        dtype=np.double
      ),
      "torso": spaces.Box(
        low= np.double(np.array(torso_low)),
        high=np.double(np.array(torso_high)),
        dtype=np.double
      )
    })

    # Initialize motors
    self.motor_drivers = []
    for m in self.MOTORS:
      props = yarp.Property()
      props.put("device", "remote_controlboard")
      props.put("local", m[3])
      props.put("remote", m[2])

      driver = yarp.PolyDriver(props)
      iPos = driver.viewIPositionControl()
      iVel = driver.viewIVelocityControl()
      iEnc = driver.viewIEncoders()

      self.motor_drivers.append({
        "driver": driver,
        "iPos": iPos,
        "iVel": iVel,
        "iEnc": iEnc
      })

    # Track episode termination
    self.step_counter = 0

    # Reset reward lookup table
    self.reset_reward_table()

    return

  # Reset reward lookup table - set false for each taxel
  def reset_reward_table(self):
    self.reward_lookup_table = {}
    for bp in self.BODY_PARTS:
      name = bp[0]
      n_taxels = bp[1]
      self.reward_lookup_table[name] = [False for i in range(n_taxels)]
  
  # Get Center-of-Mass of activated taxels for a body part
  # Here "mass" is taxel activation strength (0..255)
  def get_com(self, taxels, skin_coordinates):
    total_mass = 0
    total_x = 0
    total_y = 0
    for i in range(len(taxels)):
      mass = taxels[i]
      total_mass += mass
      total_x += mass * skin_coordinates[1][i]
      total_y += mass * skin_coordinates[2][i]
    if total_mass == 0:
      return [0, 0]
    return [total_x / total_mass, total_y / total_mass]

  def step(self, action):
    # Apply action
    self.setMotorPose(action)

    # Prepare observation
    observation = {
      "joints": {
        "head": [],
        "left_arm": [],
        "right_arm": [],
        "torso": []
      },
      "skin": {
        "left_arm": [],
        "left_forearm": [],
        "left_hand": [],
        "right_arm": [],
        "right_forearm": [],
        "right_hand": [],
        "torso": []
      },
      "touch": {
        "left_arm": [0, 0],
        "left_forearm": [0, 0],
        "left_hand": [0, 0],
        "right_arm": [0, 0],
        "right_forearm": [0, 0],
        "right_hand": [0, 0],
        "torso": [0, 0]
      }
    }

    for i in range(len(self.MOTORS)):
      m = self.MOTORS[i]
      motorName = m[0]
      numJoints = m[1]
      encs = yarp.Vector(numJoints)
      self.motor_drivers[i]['iEnc'].getEncoders(encs.data())
      observation['joints'][motorName] = np.array([encs[j] for j in range(numJoints)])

    for i in range(len(self.BODY_PARTS)):
      bp = self.BODY_PARTS[i]
      btl = self.input_ports[i].read(True)
      observation['skin'][bp[0]] = [btl.get(j).asFloat64() for j in range(btl.size())]
      observation['touch'][bp[0]] = self.get_com(observation['skin'][bp[0]], self.SKIN_COORDINATES[i])

    # Calculate solved condition
    self.step_counter += 1
    done = self.step_counter >= self.metadata['maxEpisodeSteps']

    # Calculate reward
    reward = 0
    for bp in self.BODY_PARTS:
      name = bp[0]
      n_taxels = bp[1]
      for i in range(n_taxels):
        if (not self.reward_lookup_table[name][i]) and observation['skin'][name][i]:
          self.reward_lookup_table[name][i] = True
          reward += 1

    return observation, reward, done, {}


  def reset(self):
    # Set movement speeds for all joints
    for i in range(len(self.MOTORS)):
      m = self.MOTORS[i]
      numJoints = m[1]
      speeds = yarp.Vector(numJoints)
      for j in range(numJoints):
        speeds[j] = self.metadata['jointSpeed'];
      self.motor_drivers[i]['iPos'].setRefSpeeds(speeds.data())

    # Move robot to home pose
    self.setMotorPose(self.HOME_POSE)

    # Reset step counter
    self.step_counter = 0

    # Reset reward lookup table
    self.reset_reward_table()


  # Dummy method
  # Rendering is actually done in Gazebo simulator
  def render(self, mode='terminal'):
    # TODO: maybe print joint values and some skin information to terminal here?
    pass

  def close(self):
    # Disconnect ports
    for bp in self.BODY_PARTS:
      yarp.Network.disconnect(bp[3], bp[2])
    return

  def setMotorPose(self, action):
    # Apply action - move motors
    targets = []
    for i in range(len(self.MOTORS)):
      m = self.MOTORS[i]
      motorName = m[0]
      numJoints = m[1]
      if motorName in self.metadata['activeBodyParts']:
        angles = yarp.Vector(numJoints)
        for j in range(numJoints):
          angles[j] = action[motorName][j]
        targets.append(np.array(action[motorName]))
        self.motor_drivers[i]['iPos'].positionMove(angles.data())

    # Wait until movement is stopped or maxMoveDuration seconds expire
    timer = 0
    dt = self.metadata['moveTimeStep']
    
    # Store previous motor state
    v_prev = []
    for i in range(len(self.MOTORS)):
      m = self.MOTORS[i]
      motorName = m[0]
      numJoints = m[1]
      if motorName in self.metadata['activeBodyParts']:
        encs = yarp.Vector(numJoints)
        self.motor_drivers[i]['iEnc'].getEncoders(encs.data())
        v_prev.append(np.array([encs[j] for j in range(numJoints)]))
      else:
        v_prev.append(None)

    while timer < self.metadata['maxMoveDuration']:
      time.sleep(dt)
      timer += dt
      all_good = True
      for i in range(len(self.MOTORS)):
        m = self.MOTORS[i]
        motorName = m[0]
        numJoints = m[1]
        if motorName in self.metadata['activeBodyParts']:
          encs = yarp.Vector(numJoints)
          self.motor_drivers[i]['iEnc'].getEncoders(encs.data())
          v = np.array([encs[j] for j in range(numJoints)])
          v_norm = np.linalg.norm(v - v_prev[i]) # (v - targets[i])
          if (v_norm > self.metadata['moveTerminationEpsilon']):
            all_good = False
          v_prev[i] = v
      if (all_good):
        break

# Test
if __name__ == "__main__":
  env = IcubSkinEnv()
  env.reset()
  observation, reward, done, info = env.step({
    "head":      np.double(np.array([0, 0, 0, 0, 0, 0])),
    "left_arm":  np.double(np.array([-30, 30, 0, 60, 0, 0, 0, 60, 0, 0, 0, 0, 0, 0, 0, 0])),
    "right_arm": np.double(np.array([-30, 30, 0, 45, 0, 0, 0, 60, 0, 0, 0, 0, 0, 0, 0, 0])),
    "torso":     np.double(np.array([0, 0, 0]))
  })
  print(observation)

