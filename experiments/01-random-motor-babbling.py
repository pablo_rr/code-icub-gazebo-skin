# General imports
import numpy as np
import pickle
from operator import add

# ROS and Gazebo imports
from std_srvs.srv import Empty

# Explauto imports
from explauto import Environment
from explauto import SensorimotorModel
from explauto.sensorimotor_model import sensorimotor_models, available_configurations
from explauto.sensorimotor_model.non_parametric import NonParametric

# OpenAI gym imports
from icub_skin_env import IcubSkinEnv

# Utils
from functions import *

# # # # # # # # # # # # # # # # # # # # # # # # #

# OpenAI gym environment
env = IcubSkinEnv()
env.reset()

for t in range(1000):
  print('Step:', t)
  
  # Generate random action
  action = action_random(env)
  # action = action_home(env)

  # Perform action
  observation, reward, done, info = env.step(action)

  print(reward, observation['touch'])

