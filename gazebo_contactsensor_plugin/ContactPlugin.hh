#ifndef _GAZEBO_CONTACT_PLUGIN_HH_
#define _GAZEBO_CONTACT_PLUGIN_HH_

#include "Contact2Sensor.hh"
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include "gazebo/transport/Node.hh"
#include "gazebo/transport/Subscriber.hh"
#include <gazebo/transport/transport.hh>
#include <gazebo/msgs/msgs.hh>

#include <yarp/os/Bottle.h>
#include <yarp/os/Network.h>
#include <yarp/os/Port.h>
#include <yarp/os/Time.h>

#include <string>
#include <iostream>
#include <regex>
#include <math.h>
#include <cstring>

#define N_BODY_PARTS 7
#define N_TAXELS_TORSO 768
#define N_TAXELS_ARM 768
#define N_TAXELS_FOREARM 384
#define N_TAXELS_HAND 192
#define MAX_TAXEL_FORCE 0.1
#define TAXEL_DEFAULT_VALUE 0.0

using yarp::os::Bottle;
using yarp::os::Network;
using yarp::os::Port;

namespace gazebo {
  class ContactPlugin : public ModelPlugin {
    private: physics::ModelPtr model;
    private: event::ConnectionPtr updateConnection;
    private: transport::SubscriberPtr sub;
    private: common::Time lastSendTime;

    private: std::vector<double> skin[N_BODY_PARTS];

    private: int taxelCounts[N_BODY_PARTS] = {
      N_TAXELS_ARM,
      N_TAXELS_FOREARM,
      N_TAXELS_HAND,
      N_TAXELS_ARM,
      N_TAXELS_FOREARM,
      N_TAXELS_HAND,
      N_TAXELS_TORSO
    };
    private: std::string portNames[N_BODY_PARTS] = {
      "/icubSim/skin/left_arm_comp",
      "/icubSim/skin/left_forearm_comp",
      "/icubSim/skin/left_hand_comp",
      "/icubSim/skin/right_arm_comp",
      "/icubSim/skin/right_forearm_comp",
      "/icubSim/skin/right_hand_comp",
      "/icubSim/skin/torso_comp"
    };
    private: std::string collisionPatterns[N_BODY_PARTS] = {
      "iCub::left_upper_arm_skin::left_upper_arm_skin_collision_([0-9]+).*",
      "iCub::left_forearm_skin::left_forearm_skin_collision_([0-9]+).*",
      "iCub::left_hand_skin::left_hand_skin_collision_([0-9]+).*",
      "iCub::right_upper_arm_skin::right_upper_arm_skin_collision_([0-9]+).*",
      "iCub::right_forearm_skin::right_forearm_skin_collision_([0-9]+).*",
      "iCub::right_hand_skin::right_hand_skin_collision_([0-9]+).*",
      "iCub::chest_skin::chest_skin_collision_([0-9]+).*"
    };
    private: Port outputPorts[N_BODY_PARTS];

    public: ContactPlugin();

    public: virtual ~ContactPlugin();

    public: virtual void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf);

    private: virtual void OnUpdate();
    
    private: void updateLastSendTime();
    
    public: void OnContact(ConstContactsPtr &_msg);

    public: virtual void Reset();

  };
}
#endif

