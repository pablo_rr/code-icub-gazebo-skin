#include "ContactPlugin.hh"

using namespace gazebo;
using namespace transport;

using yarp::os::Bottle;
using yarp::os::Network;
using yarp::os::Port;

ContactPlugin::ContactPlugin() : ModelPlugin() {
  
}

ContactPlugin::~ContactPlugin() {
  std::cout << "Contact plugin shutting down" << std::endl;
}

void ContactPlugin::updateLastSendTime() {
  this->lastSendTime = this->model->GetWorld()->RealTime();
}

void ContactPlugin::Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/) {
  std::cout << "Contact plugin loading" << std::endl;

  // Check YARP network
  // Network yarp;
  // if (!yarp.checkNetwork()) {
  //   printf("No YARP Network!\n");
  //   return;
  // }

  // Store reference to the model
  this->model = _parent;

  // Set time of last sent skin data
  this->updateLastSendTime();

  // Setup YARP ports
  for (int i = 0; i < N_BODY_PARTS; i++) {
    this->outputPorts[i].open(this->portNames[i]);
    
    this->skin[i].resize(this->taxelCounts[i]);
    std::fill(this->skin[i].begin(), this->skin[i].end(), TAXEL_DEFAULT_VALUE);
  }
  // Network::connect("/icubSim/skin/torso", "/skinGui/torso_virtual:i");

  // Listen to the update event
  this->updateConnection = event::Events::ConnectWorldUpdateBegin(
    std::bind(&ContactPlugin::OnUpdate, this)
  );

  // Subscribe to ~/physics/contacts topic
  NodePtr node(new Node());
  node->Init();
  this->sub = node->Subscribe("~/physics/contacts", &ContactPlugin::OnContact, this);

  // Force show contact visualization in Gazebo
  PublisherPtr requestPub = node->Advertise<msgs::Request>("~/request");
  requestPub->WaitForConnection();
  msgs::Request *request = msgs::CreateRequest("show_contact");
  requestPub->Publish(*request);
}

void ContactPlugin::OnContact(ConstContactsPtr &_msg) {
  // Debug output to check subsription is working
  // std::cout << _msg->DebugString();

  if (_msg->contact_size() > 0) {
    int timeSec = _msg->time().sec();
    int timeNsec = _msg->time().nsec();

    // std::cout << "[" << timeSec << "." << timeNsec << "] Contacts: " << _msg->contact_size() << std::endl;

    for (unsigned int i = 0; i < _msg->contact_size(); ++i) {
      std::cmatch cm;
      std::string collisionName1 = _msg->contact(i).collision1();
      std::string collisionName2 = _msg->contact(i).collision2();
      
      for (int j = 0; j < N_BODY_PARTS; j++) {
        std::regex_match(collisionName1.c_str(), cm, std::regex(this->collisionPatterns[j]));
        if (!cm.size()) {
          std::regex_match(collisionName2.c_str(), cm, std::regex(this->collisionPatterns[j]));
        }
        if (cm.size()) {
          int taxelNumber = stoi(cm[1]);
          msgs::Vector3d f = _msg->contact(i).wrench(0).body_1_wrench().force();
          double force = sqrt(f.x() * f.x() + f.y() * f.y() + f.z() * f.z());
          double taxelValue = std::min(
            255.0,
            ((force / MAX_TAXEL_FORCE) * 255.0)
          );

          // std::cout << "Force: " << force << ", taxel: " << taxelValue << std::endl;
          this->skin[j][taxelNumber] = taxelValue;
        }
      }
    }
  }
}

void ContactPlugin::OnUpdate() {
  // Get time difference from last skin data
  common::Time diff = this->model->GetWorld()->RealTime() - this->lastSendTime;
  
  // Send skin data at 10 Hz
  // For 50 Hz, use nsec value 20000000
  // TODO: make this configurable, so there is no need to recompile the library in the future
  if (diff.sec > 0 || diff.nsec >= 100000000) {
    // std::cout << diff << std::endl;

    // Update last time
    this->updateLastSendTime();
    
    // Send YARP data
    for (int i = 0; i < N_BODY_PARTS; i++) {
      Bottle bot;
      for (int j = 0; j < this->taxelCounts[i]; j++) {
        bot.addFloat64(this->skin[i][j]);
      }
      this->outputPorts[i].write(bot);

      // Nullify taxel data
      std::fill(this->skin[i].begin(), this->skin[i].end(), TAXEL_DEFAULT_VALUE);
    }
  }
}

void ContactPlugin::Reset() {
  this->updateLastSendTime();
}

GZ_REGISTER_MODEL_PLUGIN(ContactPlugin)

