const makePattern = 'blue';

const lines = [
   84, 600,
   72, 108,  96, 672, 732, 720, 588,
   60, 120, 132, 684, 696, 708, 744,
  324, 312, 300, 264, 276, 624, 408,
  204, 336, 348, 288, 480, 540, 528, 396, 384,
  216, 228, 468, 456, 492, 504, 516, 552, 564,
  420, 432, 444
];

lines.sort((a, b) => a - b);
lines.forEach(lineNum => {
  for (let i = 0; i < 12; i++) {
    // console.log(`${lineNum + i} `);
  }
});

const producePattern = (pattern, i, x, y) => {
  const lines = pattern.split('\n').map(line => line.trim()).filter(x => x !== '').map(line => line.split(' ').map(Number));
  
  for (let j = 0; j < 12; j++) {
    const dx = lines[j][1] - lines[0][1];
    const dy = lines[j][2] - lines[0][2];
    console.log(`${i+j} ${x+dx} ${y+dy}`);
  }
};

// Black pattern
if (makePattern === 'black') {
  const patternBlack = `
    60 137 186
    61 144 172
    62 151 186
    63 144 198
    64 159 198
    65 166 210
    66 159 205
    67 151 210
    68 137 210
    69 124 210
    70 130 205
    71 130 198
  `;
  [
    [108, 172, 124],
    [324, 137, 247],
    [456, 210, 369],
    [480, 246, 306],
    [528, 315, 306],
    [672, 245, 124],
    [720, 314, 124],
  ].forEach(([i, x, y]) => {
    producePattern(patternBlack, i, x, y);
  });
}

// Yellow pattern
if (makePattern === 'yellow') {
  const patternYellow = `
    708 330 190
    709 322 202
    710 316 190
    711 322 178
    712 308 178
    713 300 166
    714 308 170
    715 315 166
    716 330 166
    717 342 166
    718 336 170
    719 336 178
  `;
  [
    [264, 257, 251],
    [288, 223, 312],
    [336, 152, 312],
    [516, 330, 374],
    [564, 400, 374],
    [708, 330, 190],
  ].forEach(([i, x, y]) => {
    producePattern(patternYellow, i, x, y);
    console.log();
  });
}

// Red pattern
if (makePattern === 'red') {
  const patternRed = `
    72 152 104
    73 166 104
    74 158 116
    75 144 116
    76 152 128
    77 144 140
    78 144 132
    79 138 128
    80 130 116
    81 124 104
    82 130 108
    83 138 104
  `;
  [
    [ 72, 152, 104],
    [ 96, 224, 104],
    [312, 186, 226],
    [396, 364, 288],
    [468, 190, 350],
    [540, 294, 288],
    [588, 364, 104],
    [624, 328, 226],
    [732, 294, 104],
  ].forEach(([i, x, y]) => {
    producePattern(patternRed, i, x, y);
    console.log();
  });
}

// Green pattern
if (makePattern === 'green') {
  const patternGreen = `
    84 158 74
    85 166 86
    86 152 86
    87 144 74
    88 138 86
    89 124 86
    90 130 83
    91 130 74
    92 138 64
    93 144 50
    94 144 60
    95 152 63
  `;
  [
    [ 84, 158,  74],
    [300, 230, 258],
    [384, 406, 320],
    [408, 370, 260],
    [600, 370,  74],
  ].forEach(([i, x, y]) => {
    producePattern(patternGreen, i, x, y);
    console.log();
  });
}

// White pattern
if (makePattern === 'white') {
  const patternWhite = `
    120 166 178
    121 158 166
    122 172 166
    123 180 178
    124 186 166
    125 202 166
    126 194 170
    127 194 178
    128 186 190
    129 180 202
    130 180 194
    131 172 190
  `;
  [
    [120, 166, 178],
    [216,  96, 360],
    [420, 204, 423],
    [444, 272, 423],
    [492, 240, 360],
    [684, 240, 178],
  ].forEach(([i, x, y]) => {
    producePattern(patternWhite, i, x, y);
    console.log();
  });
}

// Blue pattern
if (makePattern === 'blue') {
  const patternBlue = `
    132 208 210
    133 194 210
    134 200 198
    135 216 198
    136 208 186
    137 216 172
    138 216 182
    139 222 186
    140 230 198
    141 236 210
    142 230 206
    143 222 210
  `;
  [
    [132, 208, 210],
    [204, 102, 332],
    [228, 138, 392],
    [276, 278, 270],
    [348, 172, 332],
    [432, 246, 454],
    [504, 280, 392],
    [552, 350, 392],
    [696, 280, 210],
    [744, 350, 210],
  ].forEach(([i, x, y]) => {
    producePattern(patternBlue, i, x, y);
    console.log();
  });
}
